# Notepad dokuwiki template

* Based on a wordpress theme
* Designed by [N Design Studio](https://wordpress.org/themes/notepad-theme/)
* Converted by [desbest](http://desbest.com)
* Metadata is in template.info.txt
* Under the GPL license (see copying file)
* [More information](http://dokuwiki.org/template:notepad)

![able theme screenshot](https://i.imgur.com/qJm4H7z.png)